//
//  DateFormatter.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

enum DateFormat: String {
    case Date_WithDash_yMd = "yyyy-MM-dd"
}

extension String {
    // Convert String to Date
    func convertStringToDate(_ formatString: DateFormat) -> Date {
        let formatter = DateFormatter() // Create Time Formatter
        formatter.dateFormat = formatString.rawValue // Specify Format of Date to Parse
        let dateFromString:Date = formatter.date(from: self) ?? Date() // Parse into Date
        return dateFromString // Return Parsed Date
    }
    
    func convertStringToNextDay(_ formatString: DateFormat) -> String {
        let dateFromString = convertStringToDate(formatString) // Parsed Date
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: dateFromString) ?? Date() // Next Date
        let nextDateString = nextDate.convertDateToString(formatString) // Next Date String
        return nextDateString // Return Next Date String
    }
}

extension Date {
    // Convert Date to String
    func convertDateToString(_ formatString: DateFormat) -> String {
        let formatter = DateFormatter() // Create Date Formatter
        formatter.dateFormat = formatString.rawValue // Specify Format of String to Parse
        let stringFromDate:String = formatter.string(from: self) as String  // Parse into String
        return stringFromDate // Return Parsed String
    }
}

