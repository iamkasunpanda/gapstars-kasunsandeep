//
//  Constants.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/9/20.
//

import UIKit
import SwiftyBeaver

let log = SwiftyBeaver.self

struct Constants {
    static var customHeaders: [String: String] = ["Accept":"application/json"]
    
    //MARK: Get URLs (Base url etc.)
    enum URLs {
        static let baseUrl = "https://temper.works/api/v1/"
    }
}
