//
//  Localizer.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import UIKit

extension String {
    static let InternetConnectionOffline = NSLocalizedString("Internet connection appears to be offline.", comment: "")
    static let ErrorCorrupted = NSLocalizedString("Error is corrupted.", comment: "")
    static let MissingData = NSLocalizedString("Missing data in the request.", comment: "")
}
