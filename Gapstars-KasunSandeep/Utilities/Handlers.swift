//
//  Handlers.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import UIKit

typealias ActionHandler = (_ status: Bool, _ message: String) -> ()
typealias CompletionHandler = (_ status: Bool, _ code: Int, _ message: String) -> ()
