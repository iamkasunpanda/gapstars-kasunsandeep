//
//  BaseVM.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import Foundation
import SwiftyJSON


class BaseVM {
    //Handle API Error
    func hadleErrorResponse(_ error: Error?, completion: CompletionHandler) {
        
        if let errorResponse = error as? ErrorResponse {
            switch errorResponse {
            case .error(let statusCode, let data, _):
                guard let responseData = data else {return}
                let errorJson = JSON(responseData)
                completion(false, statusCode, errorJson["message"].stringValue)
            }
        } else {
            completion(false, 422, .ErrorCorrupted)
        }
    }
}
