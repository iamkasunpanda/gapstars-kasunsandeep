//
//  ShiftsVM.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import UIKit
import RxSwift
import RxDataSources

typealias SectionOfShifts = SectionModel<String, ShiftContainerModel>

class ShiftsVM: BaseVM {
    var shiftDataList: [SectionOfShifts] = []
    let shiftList = PublishSubject<[SectionOfShifts]>()

    //MARK: Proceed with shiftsGet API
       func shiftsGetNetworkRequest(date: String, completion: @escaping CompletionHandler) {
           //check internet connection
           guard Reachability.isInternetAvailable() else {
               completion(false, 503, .InternetConnectionOffline)
               return
           }
           
            ShiftAPI.shiftsGet(date: date) { [weak self] (response, error) in
                guard error == nil else {
                    //handle error
                    self?.hadleErrorResponse(error, completion: { (status, statusCode, message) in
                        completion(status, statusCode, message)
                    })
                    return
                }
                
                guard let responseJson = response?.data else {
                    completion(false, 500, .MissingData)
                    return
                }
                
                if let shifts = (responseJson.value as! [String:[AnyObject]])[date]?.map({ShiftContainerModel(from: $0)}) {
                    self?.shiftDataList += [SectionOfShifts(model: date, items: shifts)]
                    self?.shiftList.onNext(self?.shiftDataList ?? [])
                }
                
                completion(true, 200, "success")
            }
       }
}
