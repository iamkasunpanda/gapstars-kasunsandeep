//
//  ShiftsVC.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/9/20.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa
import PullToRefreshKit

class ShiftsVC: BaseVC {
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var kaartBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    // MARK: Variables
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    let vm = ShiftsVM()
    private let bag = DisposeBag()
    
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfShifts>(configureCell: configureCell, titleForHeaderInSection: configureSectionHeader)
    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfShifts>.ConfigureCell = { [unowned self] (dataSource, tableView, indexPath, item) in
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "ShiftsTVCell", for: indexPath) as? ShiftsTVCell else {
            return UITableViewCell()
        }
        cell.configCell(model: item)
        return cell
    }
    private lazy var configureSectionHeader: RxTableViewSectionedReloadDataSource<SectionOfShifts>.TitleForHeaderInSection = { [unowned self] (dataSource, section) in
        return dataSource.sectionModels[section].model
    }
     
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setObservers()
        shiftsGetNetworkRequest()
    }
    
    // MARK: Functions
    private func setupUI() {
        tableView.estimatedRowHeight = 100
        tableView.rx.setDelegate(self).disposed(by: bag)
        
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.black.cgColor
    }
    
    private func setObservers() {
        vm.shiftList
        .bind(to: tableView.rx.items(dataSource: dataSource))
        .disposed(by: bag)
        
        self.tableView.configRefreshFooter(container: self) {
            self.shiftsGetNetworkRequest()
        }
        
        kaartBtn.rx.tap
        .subscribe() {[weak self] event in
            self?.showMapViewAction()
        }
        .disposed(by: bag)
        
        signUpBtn.rx.tap
        .subscribe() {[weak self] event in
            self?.signupAction()
        }
        .disposed(by: bag)
        
        loginBtn.rx.tap
        .subscribe() {[weak self] event in
            self?.loginAction()
        }
        .disposed(by: bag)
    }

    //MARK: Network Requests
    func shiftsGetNetworkRequest() {
        var date = ""
        if let lastSection = dataSource.sectionModels.last?.model {
            date = lastSection.convertStringToNextDay(.Date_WithDash_yMd)
        } else {
            date = Date().convertDateToString(.Date_WithDash_yMd)
        }
        
        startLoading()
        vm.shiftsGetNetworkRequest(date: date, completion: { [weak self] (status, statusCode, message) in
            self?.stopLoading()
            self?.tableView.switchRefreshFooter(to: .normal)
        })
    }
    
    //MARK: Actions
    func showMapViewAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "MapVC")
        if let _destVc = destVc as? MapVC {
            _destVc.shiftDataList = vm.shiftDataList
        }
        navigationController?.pushViewController(destVc, animated: true)
    }
    
    func signupAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "SignupVC")
        navigationController?.pushViewController(destVc, animated: true)
    }
    
    func loginAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "LoginNC")
        appDelegate.setAsRoot(_controller: destVc)
    }
}

// MARK: Outlets
extension ShiftsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
