//
//  ShiftsTVCell.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/9/20.
//

import UIKit
import AlamofireImage

class ShiftsTVCell: UITableViewCell {
    // MARK: Outlets
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var costContainerView: UIView!
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    // MARK: LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {}
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {}
    
    // MARK: Functions
    private func setupUI() {
        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 0.5)) {
            self.costContainerView.roundCorners(corners: .topLeft, radius: 5)
        }
    }

    func configCell(model: ShiftContainerModel) {
        if let imageUrlStr = model.client?.photos?.first?.formats?.first?.cdnUrl, let imageUrl = URL(string: imageUrlStr) {
            bannerImg.af_setImage(withURL: imageUrl, placeholderImage: UIImage(named: "placeholder"), runImageTransitionIfCached: true, completion: nil)
        } else {
            bannerImg.image = UIImage(named: "placeholder")
        }
        
        distanceLbl.text = "SERVING - \(model.distance ?? "")"
        
        if let shift = model.shifts?.first {
            costLbl.text = "\(shift.currency ?? "") \(shift.earningsPerHour ?? 0)"
            timeLbl.text = "\(shift.startTime ?? "") - \(shift.endTime ?? "")"
        } else {
            costLbl.text = "N/A"
            timeLbl.text = "N/A"
        }
        titleLbl.text = model.title
    }
}


