//
//  MapVC.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit
import MapKit

class MapVC: BaseVC {
    //MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: Variables
    var shiftDataList: [SectionOfShifts] = []
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    //MARK: Functions
    private func setupUI() {
        loadAnnotations()
    }
    
    func loadAnnotations() {
        for shiftList in shiftDataList {
            for shift in shiftList.items {
                guard let latitudeStr = shift.location?.latitude, let latitude = Double(latitudeStr), let longitudeStr = shift.location?.longitude, let longitude = Double(longitudeStr) else {
                    continue
                }
                
                let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                
                let annotation = Anootation(shift: shift, title: shift.title ?? "", coordinate: coordinate)
                mapView.addAnnotation(annotation)
            }
        }
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
}

//MARK: Mapview deleagte
extension MapVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // Navigate to shift detail view
    }
}
