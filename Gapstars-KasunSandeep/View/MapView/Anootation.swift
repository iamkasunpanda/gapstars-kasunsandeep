//
//  Anootation.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import Foundation
import MapKit

class Anootation: NSObject, MKAnnotation {
    //MARK: Variables
    let shift: ShiftContainerModel?
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    var subtitle: String? {
        return title
    }
    
    //MARK: Lifecycle
    init(shift: ShiftContainerModel?, title: String, coordinate: CLLocationCoordinate2D) {
        self.shift = shift
        self.title = title
        self.coordinate = coordinate
        
        super.init()
    }
    
    
}
