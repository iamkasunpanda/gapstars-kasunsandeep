//
// AuthRegisterResponse.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ShiftResponse: Codable {
    public var data: AnyCodable?
    public var meta: MetaModel?

    public init(data: AnyCodable?, meta: MetaModel?) {
        self.data = data
        self.meta = meta
    }


}

