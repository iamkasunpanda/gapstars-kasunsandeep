//
//  ApiVersionModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import Foundation

public struct ApiVersionModel: Codable {

    public var current: Double?
    public var latest: Double?
    public var released: String?
    public var deprecationDate: AnyCodable?

    public init(current: Double?, latest: Double?, released: String?, deprecationDate: AnyCodable?) {
        self.current = current
        self.latest = latest
        self.released = released
        self.deprecationDate = deprecationDate
    }

    public enum CodingKeys: String, CodingKey {
        case current = "current"
        case latest = "latest"
        case released = "released"
        case deprecationDate = "deprecation_date"
    }
}
