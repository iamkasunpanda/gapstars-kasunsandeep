//
//  ShiftModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

public struct ShiftModel: Codable {
    public var _id: String?
    public var tempersNeeded: Int?
    public var earningsPerHour: Double?
    public var duration: Int?
    public var currency: String?
    public var startDate: String?
    public var startTime: String?
    public var startDatetime: String?
    public var endTime: String?
    public var endDatetime: String?
    public var isAutoAcceptedWhenAppliedFor: Int?

    public init(_id: String?, tempersNeeded: Int?, earningsPerHour: Double?, duration: Int?, currency: String?, startDate: String?, startTime: String?, startDatetime: String?, isAutoAcceptedWhenAppliedFor: Int?) {
        self._id = _id
        self.tempersNeeded = tempersNeeded
        self.earningsPerHour = earningsPerHour
        self.duration = duration
        self.currency = currency
        self.startDate = startDate
        self.startTime = startTime
        self.startDatetime = startDatetime
        self.isAutoAcceptedWhenAppliedFor = isAutoAcceptedWhenAppliedFor
    }

    public enum CodingKeys: String, CodingKey {
        case _id = "id"
        case tempersNeeded = "tempers_needed"
        case earningsPerHour = "earnings_per_hour"
        case duration = "duration"
        case currency = "currency"
        case startDate = "start_date"
        case startTime = "start_time"
        case startDatetime = "start_datetime"
        case endTime = "end_time"
        case endDatetime = "end_datetime"
        case isAutoAcceptedWhenAppliedFor = "is_auto_accepted_when_applied_for"
    }
    
    init(from: AnyObject) {
        self._id        = from["id"] as? String
        self.tempersNeeded   = from["tempers_needed"] as? Int
        self.earningsPerHour   = from["earnings_per_hour"] as? Double
        self.duration   = from["duration"] as? Int
        self.currency   = from["currency"] as? String
        self.startDate   = from["start_date"] as? String
        self.startTime   = from["start_time"] as? String
        self.startDatetime   = from["start_datetime"] as? String
        self.endTime   = from["end_time"] as? String
        self.endDatetime   = from["end_datetime"] as? String
        self.isAutoAcceptedWhenAppliedFor   = from["is_auto_accepted_when_applied_for"] as? Int
    }
}

