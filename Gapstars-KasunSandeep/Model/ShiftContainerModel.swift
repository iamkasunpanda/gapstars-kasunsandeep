//
//  ShiftContentModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/10/20.
//

import UIKit

public struct ShiftContainerModel: Codable {

    public var _id: Double?
    public var key: String?
    public var title: String?
    public var location: LocationModel?
    public var distance: String?
    public var client: ClientModel?
    public var shifts: [ShiftModel]?

    public init(id: Double?, key: String?, title: String?, location: LocationModel?, distance: String?, client: ClientModel?, shifts: [ShiftModel]?) {
        self._id = id
        self.key = key
        self.title = title
        self.location = location
        self.distance = distance
        self.client = client
        self.shifts = shifts
    }

    public enum CodingKeys: String, CodingKey {
        case _id = "id"
        case key = "key"
        case title = "title"
        case location = "location"
        case distance = "distance"
        case client = "client"
        case shifts = "shifts"
    }
    
    init(from: AnyObject) {
        self._id      = from["id"] as? Double
        self.key      = from["key"] as? String
        self.title    = from["title"] as? String
        self.location = LocationModel(from: from["location"] as AnyObject)
        self.distance = from["distance"] as? String
        self.client   = ClientModel(from: from["client"] as AnyObject)
        self.shifts   = (from["shifts"] as! [AnyObject]).map({ShiftModel(from: $0)})
    }
}
