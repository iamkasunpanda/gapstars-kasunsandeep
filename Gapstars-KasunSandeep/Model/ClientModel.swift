//
//  ClientModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

public struct ClientModel: Codable {
    public var _id: String?
    public var name: String?
    public var photos: [PhotoModel]?

    public init(_id: String?, name: String?, photos: [PhotoModel]?) {
        self._id = _id
        self.name = name
        self.photos = photos
    }

    public enum CodingKeys: String, CodingKey {
        case _id = "id"
        case name = "name"
        case photos = "photos"
    }
    
    init(from: AnyObject) {
        self._id        = from["id"] as? String
        self.name       = from["name"] as? String
        self.photos     = (from["photos"] as! [AnyObject]).map({PhotoModel(from: $0)})
    }
}
