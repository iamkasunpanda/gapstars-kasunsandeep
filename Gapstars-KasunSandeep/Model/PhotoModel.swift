//
//  PhotoModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

public struct PhotoModel: Codable {
    public var formats: [FormatModel]?

    public init(formats: [FormatModel]?) {
        self.formats = formats
    }

    public enum CodingKeys: String, CodingKey {
        case formats = "formats"
    }
    
    init(from: AnyObject) {
        self.formats = (from["formats"] as! [AnyObject]).map({FormatModel(from: $0)})
    }
}
