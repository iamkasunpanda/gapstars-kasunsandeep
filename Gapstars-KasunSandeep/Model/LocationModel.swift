//
//  LocationModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

public struct LocationModel: Codable {
    public var latitude: String?
    public var longitude: String?

    public init(latitude: String?, longitude: String?) {
        self.latitude = latitude
        self.longitude = longitude
    }

    public enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
    
    init(from: AnyObject) {
        self.latitude        = from["lat"] as? String
        self.longitude   = from["lng"] as? String
    }
}
