//
//  FormatModel.swift
//  Gapstars-KasunSandeep
//
//  Created by Kasun Sandeep on 10/11/20.
//

import UIKit

public struct FormatModel: Codable {
    public var cdnUrl: String?

    public init(cdnUrl: String?) {
        self.cdnUrl = cdnUrl
    }

    public enum CodingKeys: String, CodingKey {
        case cdnUrl = "cdn_url"
    }
    
    init(from: AnyObject) {
        self.cdnUrl = from["cdn_url"] as? String
    }
}
